# Gender Classifier

Gender Classification code with Haarcascade as detector and Fisherface as recognizer / classifier. Written in Python.

Files:
- main.py: The main program
- haarcascade file: A Haarcascade file filled with model to recognize frontal face
- data.csv: An example training data file, the format is name of file, and the label, separated by comma, and each data separated by a new line. For the label, 1 indicates Male, 2 indicates Female.