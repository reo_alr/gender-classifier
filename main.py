import cv2, cv
import csv
import numpy as np
import argparse
import time

faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

trainImage = []
trainLabels = []

def faceCrop(imageName):
	image = cv2.imread(imageName, 0)
	if image is None:
		return None
	faces = faceCascade.detectMultiScale(image)
	resVec = []
	for face in faces:
		x, y, w, h = [v for v in face]
		res = image[y:y+h, x:x+w]
		resVec.append(res)
	if len(resVec)==0:
		return None
	return cv2.resize(resVec[0], (100, 100))

def faceCropCam(frame):
	image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	if image is None:
		return None
	faces = faceCascade.detectMultiScale(image)
	resVec = []
	for face in faces:
		x, y, w, h = [v for v in face]
		res = image[y:y+h, x:x+w]
		resVec.append(res)
	if len(resVec)==0:
		return None
	return cv2.resize(resVec[0], (100, 100))

p = argparse.ArgumentParser(description="A Gender Classifier")
p.add_argument("--image", help="An image to classify")
p.add_argument("--train", help="A .csv file for training")
p.add_argument("--loadmodel", help="Load an existing model")
p.add_argument("--savemodel", help="Save a model")
args = p.parse_args()

#default
traincsv = "data.csv"
recognizer = cv2.createFisherFaceRecognizer()

if args.train:
	traincsv = args.train
if args.loadmodel:
	try:
		t = open(args.loadmodel, "rb")
	except:
		print "ERROR: Cannot load model"
		exit()
	recognizer.load(args.loadmodel)
else:
	with open(traincsv, "rb") as csvfile:
		trainRead = csv.reader(csvfile)
		for img, label in trainRead:
			read2 = faceCrop("image/"+img)
			if read2 is not None:
				trainImage.append(read2)
				trainLabels.append(np.int(label))

	print "Training data: "+str(len(trainImage))
	trainImage = np.asarray(trainImage)
	trainLabels = np.asarray(trainLabels)

	recognizer.train(trainImage, trainLabels)

if args.savemodel:
	recognizer.save(args.savemodel)

# PREDICT
if args.image:
	imgName = args.image
	imageShow = cv2.imread(imgName)
	testdata = faceCrop(imgName)
	if testdata is not None:
		result = recognizer.predict(testdata)
		if result[0]==1:
			print "Predict: Male"
		else:
			print "Predict: Female"
		cv2.imshow("Image", imageShow)
		cv2.imshow("Cropped", testdata)
		cv2.waitKey(0)
	else:
		print "Face not found in the image!"

else:
	cap = cv2.VideoCapture(0)
	if not cap.isOpened():
		print "Webcam is off"
		exit()
	while 1:
		ret, frame = cap.read()
		if ret==False:
			break
		testdata = faceCropCam(frame)
		if testdata is not None:
			result = recognizer.predict(testdata)
			if result[0]==1:
				print "Predict: Male"
			else:
				print "Predict: Female"
		else:
			print "Face not found"
		
		cv2.imshow("Cam", frame)
		if testdata is not None:
			cv2.imshow("Detected Face", testdata)
		cv2.waitKey(5)
		# time.sleep(5)